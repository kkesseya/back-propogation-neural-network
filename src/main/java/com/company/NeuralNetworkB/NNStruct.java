package com.company.NeuralNetworkB;

import com.company.MathFunctions.MatrixCalculation;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.*;


public class NNStruct {
    protected ArrayList<Double[][]> layersWeights;
    protected ArrayList<Double[][]> layersBiases;
    protected ArrayList<Double[][]> layersOutputs;
    protected ArrayList<Double[][]> layersErrors;
    protected Double weightBiasRange;
    protected ArrayList<Integer> numLayers;
    protected Double learningRate;
    protected int amtDecimals = 10000;
    protected Random rand = new Random();




    public NNStruct(Double weightBiasRange, ArrayList<Integer> numLayers, Double learningRate) {
        layersWeights = new ArrayList<Double[][]>(numLayers.size() + 1);
        layersBiases = new ArrayList<Double[][]>(numLayers.size() - 1);
        layersOutputs = new ArrayList<Double[][]>(numLayers.size() - 1);
        layersErrors = new ArrayList<Double[][]>(numLayers.size() - 1);
        this.weightBiasRange = weightBiasRange;
        this.numLayers = numLayers;
        this.learningRate = learningRate;
        initializeNetwork();
    }

    public void initializeNetwork() {
        initializeWeights();
        initializeBiases();
        initializeOutputs();
        initializeErrors();
    }

    public void initializeWeights() {//initialize weights
        Double[][] temp;
        for (int i = 0; i < numLayers.size()-1; i++) {
            temp = new Double[numLayers.get(i + 1)][numLayers.get(i)];

            for (int j = 0; j < temp.length; j++) {
                for (int k = 0; k < temp[j].length; k++) {
                    temp[j][k] = (double) Math.round((rand.nextFloat() * (weightBiasRange + weightBiasRange) - weightBiasRange)*amtDecimals)/amtDecimals;
                }
            }
            layersWeights.add(temp);
        }
        // temp = new Double[3][2];

        // temp[0][0] = 0.03453553;
        // temp[0][1] = -0.94295855;
        // temp[1][0] = -0.94155594212;
        // temp[1][1] = 0.83578843;
        // temp[2][0] = -0.63908394;
        // temp[2][1] = -0.80639244;
        // layersWeights.add(temp);

        // temp = new Double[1][3];

        // temp[0][0] = -0.13580689;
        // temp[0][1] = 0.05029915;
        // temp[0][2] = -0.01406298;
        // layersWeights.add(temp);



    }

    public void initializeBiases() {
        Double[][] temp;
         for (int i = 1; i < numLayers.size(); i++) {
             temp = new Double[numLayers.get(i)][1];
             System.out.println();
             for (int j = 0; j < temp.length; j++) {
                 for (int k = 0; k < temp[j].length; k++) {
                     temp[j][k] = (double) Math.round((rand.nextFloat() * (weightBiasRange + weightBiasRange) - weightBiasRange)*amtDecimals)/amtDecimals;
                 }
             }
             layersBiases.add(temp);
         }

    }

    public void initializeOutputs() {
        Double[][] temp;
        for (int i = 1; i < numLayers.size(); i++) {
            temp = new Double[numLayers.get(i)][1];
            layersOutputs.add(temp);
        }
    }

    public void initializeErrors(){
        Double[][] temp;
        for (int i = 1; i < numLayers.size(); i++) {
            temp = new Double[numLayers.get(i)][1];
            layersErrors.add(temp);
        }
    }

    public void updateOutputLayer(Double[][] values, int index){
        layersOutputs.set(index, values);
    }

    public void updateOutputClassificationLayer(Double[][] values){
        updateOutputLayer(values, this.numLayers.size()-1);
    }

    public void updateErrorLayer(Double[][] values, int index){
        layersErrors.set(index, values);
    }

    public void updateInputLayer(Double[][] inputVals){
        if(layersOutputs.size() < numLayers.size()){
            //append a new layer to the beginning
            layersOutputs.add(0, inputVals);
        }else { //set the 0index which would have been set from the first updateInputLayer Call
            layersOutputs.set(0, inputVals);
        }
    }

    public ArrayList<Double[][]> getLayersBiases() {
        return layersBiases;
    }

    public ArrayList<Double[][]> getLayersOutputs() {
        return layersOutputs;
    }

    public ArrayList<Double[][]> getLayersWeights() {
        return layersWeights;
    }

    public ArrayList<Integer> getNumLayers() {
        return numLayers;
    }

    public double getWeightBiasRange() {
        return weightBiasRange;
    }

    public int getNumOutputLayerNodes(){
        return numLayers.get(numLayers.size());
    }

    public int getNumOutputLayers(){
        return layersOutputs.size();
    }

    public int getNumHiddenLayers(){
        return layersOutputs.size()-2; //exclude output and input
    }

    public void print2D(Double mat[][])
    {
        // Loop through all rows
        for (Double[] row : mat)

            // converting each row as string
            // and then printing in a separate line
            System.out.println(Arrays.toString(row));
    }

    public void printNeuralNetWeights(){
        System.out.println("\n======================= Neural Network Weights =======================");
        for (int j = 0; j < this.getNumLayers().size() - 1; j++) { //computed inputs = sum(weights*prev outputlayers) + biases
                System.out.println("\nLayer #: "+(j+1));
                this.print2D(this.layersWeights.get(j));
        }
        System.out.println("\n====================================================================\n");
    }





//    public static void main(String[] args) {
//        //A rule of thumb is to set the learning rate to 1/t, where t is the number of iterations through the training set so far.
//        ArrayList<Integer> layersAndNodes = new ArrayList<Integer>();
//        layersAndNodes.add(3); //input layer
//        layersAndNodes.add(2); //hidden layer
//        layersAndNodes.add(1); //output layer
//
//        NNStruct nn = new NNStruct(1.0,layersAndNodes, 0.9);
//        LinkedHashMap<String, Double>  tupleInputs = new LinkedHashMap<String, Double>();
//        tupleInputs.put("input1", 1.0);
//        tupleInputs.put("input2", 0.0);
//        tupleInputs.put("input3", 1.0);
//
//        //outputs
//        LinkedHashMap<String, Double>  tupleOutputs = new LinkedHashMap<String, Double>();
//        tupleOutputs.put("output1", 1.0);
//
//        Tuple testTuple = new Tuple(tupleInputs, tupleOutputs);
//
//        ArrayList<Tuple> dataSet = new ArrayList<Tuple>();
//        dataSet.add(testTuple);
//
//        TrainingNN train = new TrainingNN(nn, dataSet);
//
//
//    }
}