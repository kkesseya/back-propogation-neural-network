package com.company.NeuralNetworkB;

import java.util.Collection;
import java.util.LinkedHashMap;

public class Tuple {
    protected LinkedHashMap<String, Double> outputs;
    protected LinkedHashMap<String, Double> inputs;
    protected Double[][] inputsValuesArray;
    protected Double[][] outputsValuesArray;



    public Tuple(LinkedHashMap<String, Double> inputs, LinkedHashMap<String, Double>  outputs){
        this.inputs = inputs;
        this.outputs = outputs;
        computeInputValuesArray();
        computeOutputValuesArray();

    }

    public Tuple(LinkedHashMap<String, Double> inputs){
        this.inputs = inputs;
        computeInputValuesArray();
    }



    public void computeInputValuesArray(){//creates a 2d array of the values from each domain
        Collection<Double> values = inputs.values();
        Double[] temp = values.toArray(new Double[0]);

        this.inputsValuesArray = new Double[temp.length][1];
        for(int i=0;i<temp.length;i++){
            inputsValuesArray[i][0] = temp[i];
        }
    }

    public void computeOutputValuesArray(){//creates a 2d array of the values from each domain
        Collection<Double> values = outputs.values();
        Double[] temp = values.toArray(new Double[0]);

        this.outputsValuesArray = new Double[temp.length][1];
        for(int i=0;i<temp.length;i++){
            outputsValuesArray[i][0] = temp[i];
        }
    }

    public Double[][] getOutputsValuesArray() {
        return outputsValuesArray;
    }

    public Double[][] getInputsValuesArray(){
        return inputsValuesArray;
    }
}
