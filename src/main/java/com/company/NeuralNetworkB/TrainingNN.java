package com.company.NeuralNetworkB;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import com.company.MathFunctions.*;

public class TrainingNN {
    protected List<Tuple> dataSet;
    protected NNStruct neuralNet;
    protected Double[][] knownLayer;
    protected boolean printLog = false;
    MatrixCalculation Matrix = new MatrixCalculation();
    public TrainingNN(NNStruct neuralNet, ArrayList<Tuple> dataSet){
        this.dataSet = dataSet;
        this.neuralNet = neuralNet;
        train();

    }

    public void train(){
        Double[][] outputLayer;

        for(int i=0;i<dataSet.size();i++){//iterate through each tuple in the training set

                //@TODO Add a flag to determine if each iteration should log to display

                /*
                *
                * PROPAGATE INPUTS FORWARD
                *
                */

                //set the input layer values using a tuple
                neuralNet.updateInputLayer(dataSet.get(i).getInputsValuesArray());

                //compute hidden and output layer nodes
//                System.out.println("===Forward propogation===");
                if(printLog){
                    System.out.println("Layer #: 0");
                    neuralNet.print2D(neuralNet.layersOutputs.get(0));
                }


                for(int j=0;j<neuralNet.getNumLayers().size()-1;j++){ //computed inputs = sum(weights*prev outputlayers) + biases
                    Double[][] outputMatrix;

                    //computed outputs = 1/(1+e^(- computed inputs))
                    //iterating through each layer from layer(input+1) to output

                    outputMatrix = Matrix.matricesMultiplication(neuralNet.layersWeights.get(j), neuralNet.layersOutputs.get(j));
                    outputMatrix = Matrix.matricesAddition(outputMatrix, neuralNet.layersBiases.get(j));
                    outputMatrix = activationFunction(outputMatrix);//applies the output equation to the matrix
                    if(printLog){
                        System.out.println("\nLayer #: "+(j+1));
                        neuralNet.print2D(outputMatrix);
                    }

                    neuralNet.updateOutputLayer(outputMatrix, j+1);
                }


                /*
                *
                *
                * BACK PROPAGATE THE ERRORS
                *
                */
                if(printLog){
                    System.out.println("\n\n===Back propogation===");
                }
                //for output layer compute error
                outputLayer = neuralNet.layersOutputs.get(neuralNet.layersOutputs.size()-1);// O
                Double[][] knownLayer = dataSet.get(i).getOutputsValuesArray();
                Double[][] derivLogFunc = Matrix.matricesScalarSubtraction(outputLayer, 1.0);// (1-O)
                Double[][] tenpT_O = Matrix.matricesSubtraction(knownLayer, outputLayer); // (T-O)
                Double[][] outputErrorLayer = Matrix.matricesScalarRowMultiplication(outputLayer, derivLogFunc, tenpT_O);// Error Layer = Output Layer * (one matrix - output Layer) * (Known Layer - Output Layer)
                //@TODO: update get num layers compensate for input lYER
                //@TODO: related to getting the number of output layers in the nn struct not being accurate
                neuralNet.updateErrorLayer( outputErrorLayer, neuralNet.getNumOutputLayers()-2); //(getnum indexes from 1) place value in the last layer for errors
                if(printLog){
                    System.out.println("\nOutput layer #"+(neuralNet.layersOutputs.size()-1) + " Error :"); //log
                    neuralNet.print2D(outputErrorLayer);
                }

                //  HIDDEN LAYERS
                for(int j=neuralNet.getNumHiddenLayers();j>0;j--){//from last hidden Layer to 1st hidden layer
                    Double[][] currentHiddenLayer = neuralNet.layersOutputs.get(j);
                    Double[][] sumErrorWeightMatrix;
                    //iterating through each layer from output to input
                    derivLogFunc = Matrix.matricesScalarSubtraction(currentHiddenLayer, 1.0);// (1-O)
                    sumErrorWeightMatrix = Matrix.matricesMultiplication(Matrix.transpose(neuralNet.layersWeights.get(j)), neuralNet.layersErrors.get(j));
                    Double[][] currentHiddenErrorLayer = Matrix.matricesScalarRowMultiplication(sumErrorWeightMatrix, derivLogFunc, currentHiddenLayer);
                    neuralNet.updateErrorLayer(currentHiddenErrorLayer, (j-1));

                    if(printLog){
                        System.out.println("\nHidden layer #"+j + " Error:"); //log
                        neuralNet.print2D(currentHiddenErrorLayer);
                    }

                }

                Double[][] errorLayer;

                for (int j = neuralNet.layersWeights.size()-1; j >= 0 ; j--) { //update weights using the errors from output to before input
                    outputLayer = neuralNet.layersOutputs.get(j);//Err(j)
                    errorLayer = neuralNet.layersErrors.get(j);//O(i)
                    Double[][] weightIncrementMatrix = Matrix.matricesMultiplication(errorLayer, Matrix.transpose(outputLayer));//weight increment
                    weightIncrementMatrix = Matrix.matricesScalarMultiplication( weightIncrementMatrix, neuralNet.learningRate);
                    Double[][] weightUpdateMatrix = Matrix.matricesAddition(neuralNet.layersWeights.get(j), weightIncrementMatrix);//weight update
                    neuralNet.layersWeights.set(j, weightUpdateMatrix);

                    if(printLog){
                        System.out.println("\nLayer #"+(j+1) + " weights:"); //log
                        neuralNet.print2D(weightIncrementMatrix);
                    }

                }


               //Back Propagation for Biases

               for (int j = neuralNet.layersBiases.size()-1; j >= 0 ; j--) { //update biases using the errors from output to before input
                   errorLayer = neuralNet.layersErrors.get(j);
                   Double[][] biasIncrementMatrix = Matrix.matricesScalarMultiplication(errorLayer, neuralNet.learningRate);
                   Double[][] biasUpdateMatrix = Matrix.matricesAddition(neuralNet.layersBiases.get(j), biasIncrementMatrix);
                   neuralNet.layersBiases.set(j, biasUpdateMatrix);

                   if(printLog){
                       System.out.println("\nLayer #"+(j+1) + " Bias Matrix:"); //log
                       neuralNet.print2D(biasUpdateMatrix);
                   }

               }


            }

    }
    public static Double[][] activationFunction(Double[][] matrixNetInputs){
        for (int i=0;i<matrixNetInputs.length; i++){
            for (int j = 0; j < matrixNetInputs[i].length; j++) {
                matrixNetInputs[i][j] = 1/(1+Math.exp(-1*matrixNetInputs[i][j]));
            }
        }
        return matrixNetInputs;
    }



    public static void main(String[] args) {
        //A rule of thumb is to set the learning rate to 1/t, where t is the number of iterations through the training set so far.
        ArrayList<Integer> layersAndNodes = new ArrayList<Integer>();
        
        int num_of_inputs = 1;
        int num_of_outputs = 1;
        double learning_rate = 0.3;
        double weight_range = 0.5;
        String dataSet_name = "sin(x) training set";

        layersAndNodes.add(num_of_inputs); //input layer
        layersAndNodes.add(3); //hidden layer
        layersAndNodes.add(num_of_outputs); //output layer


        NNStruct nn = new NNStruct(weight_range,layersAndNodes, learning_rate);
//        LinkedHashMap<String, Double> tupleInputs = new LinkedHashMap<String, Double>();
//        tupleInputs.put("input1", 1.0);
//        tupleInputs.put("input2", 0.0);
//        tupleInputs.put("input3", 1.0);
//        tupleInputs.put("input4", 1.0);
//
//        //outputs
//        LinkedHashMap<String, Double>  tupleOutputs = new LinkedHashMap<String, Double>();
//        tupleOutputs.put("output1", 1.0);
//
//        Tuple testTuple = new Tuple(tupleInputs, tupleOutputs);
//
//        ArrayList<Tuple> dataSet = new ArrayList<Tuple>();
//        dataSet.add(testTuple);


        //System.out.println("C:\\Users\\mahmad\\Desktop\\Personal\\side_projects\\back-propogation-neural-network\\sampleTestAndTrainingData\\ml addict training set\\training.csv");

        DatasetReader datasetReader = new DatasetReader("C:\\Users\\mahmad\\Desktop\\Personal\\side_projects\\back-propogation-neural-network\\sampleTestAndTrainingData\\" + dataSet_name + "\\training.csv", true, num_of_outputs);
        

        ArrayList<Tuple> dataSet = datasetReader.getDataSet();
        TrainingNN train = new TrainingNN(nn, dataSet);
        nn.printNeuralNetWeights();
        DatasetReader datasetReader2 = new DatasetReader("C:\\Users\\mahmad\\Desktop\\Personal\\side_projects\\back-propogation-neural-network\\sampleTestAndTrainingData\\" + dataSet_name + "\\testing.csv", false, num_of_outputs);
        ArrayList<Tuple> dataSet2 = datasetReader2.getDataSet();
        RunNeuralNetwork runNeuralNetwork = new RunNeuralNetwork(nn, dataSet2);




    }
}
