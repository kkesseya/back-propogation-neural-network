package com.company.NeuralNetworkB;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;

public class DatasetReader {
    private File dataSetCSVFile;
    private static final String delimiter = ",";
    private ArrayList<Tuple> dataSet;
    private int numOutputColumns = 0;



    public DatasetReader (String filename, boolean isTraining, int numOutputColumns) {
        //@TODO: check if file exists
        //@TODO: verify file format
        this.numOutputColumns = numOutputColumns;
        // also try to check the mime type  of the file to ensure that it is a csv
        dataSetCSVFile = new File(filename);

        boolean exists = dataSetCSVFile.exists();
        dataSet = new ArrayList<Tuple>();
    }

    public ArrayList<Tuple> getDataSet(){
        if(!dataSet.isEmpty()){
            return dataSet;
        }else{
            return createDataSet();
        }
    }

    private ArrayList<Tuple> createDataSet(){
        try {
            File file = dataSetCSVFile;
            FileReader fr = new FileReader(file);
            BufferedReader br = new BufferedReader(fr);
            String line = "";
            String[] tempArr;

            line = br.readLine();
            ArrayList<String> inputNames = new ArrayList<String>();
            for(String inputName : line.split(delimiter)) {
                inputNames.add(inputName);
            }

            while((line = br.readLine()) != null) {
                LinkedHashMap<String, Double> tupleInputs = new LinkedHashMap<String, Double>();
                LinkedHashMap<String, Double> tupleOutputs = new LinkedHashMap<String, Double>();

                //outputs
                tempArr = line.split(delimiter);
                int index = 0;

                for(String tempStr : tempArr) {
                    if(index < (tempArr.length-numOutputColumns)){
//                        System.out.println("input: "+tempStr);
                        tupleInputs.put(inputNames.get(index), Double.valueOf(tempStr));
                    }else{
//                        System.out.println("output: "+tempStr);
                        tupleOutputs.put(inputNames.get(index), Double.valueOf(tempStr));
                    }
                    index++;
                }
                Tuple tuple = new Tuple(tupleInputs,tupleOutputs);
                dataSet.add(tuple);
            }
            br.close();
            return dataSet;
        } catch(IOException ioe) {
            ioe.printStackTrace();
        }

        return null; //we failed to read the file return null
    }
}
