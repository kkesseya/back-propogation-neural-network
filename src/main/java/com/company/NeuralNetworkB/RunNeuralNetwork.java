package com.company.NeuralNetworkB;

import com.company.MathFunctions.MatrixCalculation;
import com.company.NeuralNetworkB.TrainingNN;

import java.util.ArrayList;
import java.util.List;

public class RunNeuralNetwork {
    protected List<Tuple> dataSet;
    protected NNStruct neuralNet;
    MatrixCalculation Matrix = new MatrixCalculation();
    protected boolean printLog = false;

    public RunNeuralNetwork(NNStruct neuralNet, ArrayList<Tuple> dataSet){
        this.dataSet = dataSet;
        this.neuralNet = neuralNet;
        run();
    }

    public void run() {

        for (int i = 0; i < dataSet.size(); i++) {//iterate through each tuple in the training set
            /*
             *
             * PROPAGATE INPUTS FORWARD
             *
             */

            //set the input layer values using a tuple
            neuralNet.updateInputLayer(dataSet.get(i).getInputsValuesArray());

            //compute hidden and output layer nodes
            System.out.println("===Forward propogation===");
            if(printLog){
                System.out.println("Input Units Layer #: 0");
                neuralNet.print2D(neuralNet.layersOutputs.get(0));
            }

            for (int j = 0; j < neuralNet.getNumLayers().size() - 1; j++) { //computed inputs = sum(weights*prev outputlayers) + biases
                Double[][] outputMatrix;

                //computed outputs = 1/(1+e^(- computed inputs))
                //iterating through each layer from layer(input+1) to output

                outputMatrix = Matrix.matricesMultiplication(neuralNet.layersWeights.get(j), neuralNet.layersOutputs.get(j));
                outputMatrix = Matrix.matricesAddition(outputMatrix, neuralNet.layersBiases.get(j));
                outputMatrix = TrainingNN.activationFunction(outputMatrix);//applies the output equation to the matrix
                if(printLog){
                    System.out.println("\nOutput Units Layer #: "+(j+1));
                    neuralNet.print2D(outputMatrix);
                }

                neuralNet.updateOutputLayer(outputMatrix, j + 1);

            }

            //Double[][] sub = Matrix.matricesSubtraction(dataSet.get(i).getOutputsValuesArray(), neuralNet.getLayersOutputs().get(neuralNet.getNumLayers().size() - 1));
            //Double[][] loss = Matrix.matricesMultiplication(sub, sub);


            if(true){
                System.out.println("\n=Results=   ");
                System.out.print("Expected:   ");
                neuralNet.print2D(dataSet.get(i).getOutputsValuesArray());
                System.out.print("Actual:   ");
                neuralNet.print2D(neuralNet.getLayersOutputs().get(neuralNet.getNumLayers().size() - 1));
                //System.out.print("Loss:   ");
                //neuralNet.print2D(loss);
            }

        }
    }



}
