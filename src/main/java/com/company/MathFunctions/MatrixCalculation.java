package com.company.MathFunctions;

public class MatrixCalculation {

    public Double[][] matricesMultiplication(Double[][] firstMatrix, Double[][] secondMatrix){
        if(firstMatrix[0].length != secondMatrix.length){
            System.out.println("Matrices must be of the same inward dimensions");
            return null;
        }
        Double [][] result = new Double[firstMatrix.length][secondMatrix[0].length];

        for(int i=0; i<result.length; i++) {
            for(int j=0; j<result[0].length; j++) {
                result[i][j] = 0.0;
            }
        }

        /* Loop through each and get product, then sum up and store the value */
        for (int i = 0; i < firstMatrix.length; i++) {
            for (int j = 0; j < secondMatrix[0].length; j++) {
                for (int k = 0; k < firstMatrix[0].length; k++) {
                    result[i][j] += firstMatrix[i][k] * secondMatrix[k][j];
                }
            }
        }

        return result;
    }

    public Double[][] matricesMultiplicationThree(Double[][] firstMatrix, Double[][] secondMatrix, Double[][] thirdMatrix){
        return matricesMultiplication(secondMatrix, matricesMultiplication(firstMatrix, secondMatrix));
    }
//
//    public Double[][] matricesMultiplication(Double[][]... matrices){
//        for(int i =0; i< matrices.length;i++){
//            return matricesMultiplication(secondMatrix, matricesMultiplication(firstMatrix, secondMatrix));
//        }
//    }

    public Double dotProduct(Double[][] vect_A, Double[][] vect_B)
    {
        if((vect_A[0].length != 1) || (vect_B[0].length != 1) || (vect_A.length != vect_B.length)){
            System.out.println("Dot product can not be calculated with the given dimensions");
            return null;
        }
        Double product = 0.0;

        // Loop for calculate cot product
        for (int i = 0; i < vect_A.length; i++)
            product = product + vect_A[i][0] * vect_B[i][0];
        return product;
    }

    public Double[][] matricesAddition(Double[][] firstMatrix, Double[][] secondMatrix){
        if((firstMatrix.length != secondMatrix.length) || (firstMatrix[0].length != secondMatrix[0].length)){
            System.out.println("Matrices must be of the same dimensions for addition");
            return null;
        }

        Double [][] result = new Double[firstMatrix.length][firstMatrix[0].length];
        for (int i=0;i<result.length; i++){
            for (int j = 0; j < result[i].length; j++) {
                result[i][j] = firstMatrix[i][j] + secondMatrix[i][j];
            }
        }

        return result;
    }

    public Double[][] matricesSubtraction(Double[][] firstMatrix, Double[][] secondMatrix){
        if((firstMatrix.length != secondMatrix.length) || (firstMatrix[0].length != secondMatrix[0].length)){
            System.out.println("Matrices must be of the same dimensions for subtraction");
            return null;
        }

        Double [][] result = new Double[firstMatrix.length][firstMatrix[0].length];
        for (int i=0;i<result.length; i++){
            for (int j = 0; j < result[i].length; j++) {
                result[i][j] = firstMatrix[i][j] - secondMatrix[i][j];
            }
        }

        return result;
    }

    public Double[][] matricesScalarSubtraction(Double[][] firstMatrix, Double scalarOperand){
        Double[][] resultMatrix = new Double[firstMatrix.length][firstMatrix[0].length];
        for (int i = 0; i < resultMatrix.length ; i++) {
            for (int j = 0; j < resultMatrix[i].length; j++) {
                resultMatrix[i][j] = scalarOperand - firstMatrix[i][j];
            }
        }

        return resultMatrix;
    }

    public Double[][] matricesScalarMultiplication(Double[][] firstMatrix, Double scalarOperand){
        Double[][] resultMatrix = new Double[firstMatrix.length][firstMatrix[0].length];
        for (int i = 0; i < resultMatrix.length ; i++) {
            for (int j = 0; j < resultMatrix[i].length; j++) {
                resultMatrix[i][j] = firstMatrix[i][j] * scalarOperand;
            }
        }

        return resultMatrix;
    }

    public Double[][] matricesScalarRowMultiplication(Double[][]... matrices){
        Double[][] resultMatrix = new Double[matrices[0].length][matrices[0][0].length];
        for(int i=0;i<matrices.length;i++){
            for (int j = 0; j < resultMatrix.length ; j++) {
                for (int k = 0; k < resultMatrix[j].length; k++) {
                    if(resultMatrix[j][k] == null){
                        resultMatrix[j][k] = 1.0;
                    }
                    resultMatrix[j][k] =  resultMatrix[j][k] * matrices[i][j][k];
                }
            }
        }


        return resultMatrix;
    }

    public Double[][] transpose(Double [][] matrix){
        Double[][] resultMatrix = new Double[matrix[0].length][matrix.length];
        for(int i=0; i<matrix.length; i++){
            for(int j=0; j<matrix[0].length; j++){
                resultMatrix[j][i] = matrix[i][j];
            }
        }
        return resultMatrix;
    }

//    public Double[][] multiplyMatrixRows(Double[][]... matrices){
//        //@TODO include check to ensure the are of all the same dimension
//        for(){
//
//        }
//    }
}
